# README #

### Code Test Project ###

* A small browser based application written in Python. The application allows a user to count letters pasted into a browser dialog. Output states how many instances of each letter were encountered, as in 'a' - 10 times, 'b' - 6, etc.


### How do I get set up? ###

* Clone the repo
* run 'pip install -r requirements.txt'
* run 'python manage.py syncdb'

### Who do I talk to? ###

* Brett DeAngelis
* brett.deangelis@gmail.com