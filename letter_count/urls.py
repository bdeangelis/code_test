# letter_count urls
from django.conf.urls import patterns, url


urlpatterns = patterns(
    'letter_count.views',
    url(r'^$', 'home'),
    url(r'^create_post/$', 'create_post'),
)
