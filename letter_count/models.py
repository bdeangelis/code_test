from django.db import models
from jsonfield import JSONField

# Create your models here.


class Post(models.Model):
    text = models.TextField()
    letter_count = JSONField()

    created = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        ordering = ['created']
    
    def __unicode__(self):
        return self.text