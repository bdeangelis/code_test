from django import forms
from letter_count.models import Post


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = ['created', ]
        fields = ['text']
        widgets = {
            'text': forms.TextInput(
                attrs={'id': 'post-text', 'required': True, 'placeholder': "What's the word..."}
            ),
        }