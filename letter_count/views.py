from anyjson import serialize

from django.shortcuts import render
from django.template.loader import render_to_string
from django.http import HttpResponse

from letter_count.models import Post
from letter_count.forms import PostForm




# Helper for creating JSON responses
def JsonResponse(response):
    return HttpResponse(serialize(response), mimetype="application/json")

def home(req):

    tmpl_vars = {
        'all_posts': Post.objects.reverse(),
        'form': PostForm()
    }
    return render(req, 'letter_count/index.html', tmpl_vars)

# Lower the string before processing to prevent issues with case
# Remove any white space and only get one of each letter, count each letter
def get_the_text(text_string):
    lower_string = text_string.lower()
    set_list = set(lower_string.replace(" ", ""))
    final_list = {}
    for x in set_list:
        final_list[x] = lower_string.count(x)
    return final_list


def create_post(request):
    if request.method == 'POST':
        post_text = request.POST.get('the_post')
        response_data = {}
        text_info = get_the_text(post_text)

        post = Post(text=post_text, letter_count=text_info)
        post.save()

        response_data['result'] = 'Create post successful!'
        response_data['postpk'] = post.pk
        response_data['text'] = post.text
        response_data['alpha'] = post.letter_count
        response_data['created'] = post.created.strftime('%B %d, %Y %I:%M %p')
        result_html = render_to_string('letter_count/post_feed.html', {'post': response_data})

        return JsonResponse({"result_html": result_html})

    else:
        return JsonResponse({"result_html": "this isn't happening"})


